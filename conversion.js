
exports.convertAuthor=function(reference_list){         // Function for adding names of authors and editors, reference_list has the xml data in json format that we filtered containing all the information required to convert...
  var newobj=new Array();
    for(var m=0;m<reference_list.length;m++){
        var ref=(reference_list[m]['element-citation'])?reference_list[m]['element-citation']:reference_list[m]['mixed-citation'];
        //console.log(ref);

        //console.log(ref['person-group'][i].hasOwnProperty(name));
         if(ref['person-group'] && Array.isArray(ref['person-group'])){
         var newobj2=new Array();
         for(var i=0;i<ref['person-group'].length;i++){
            //console.log(ref['person-group'][i]);
            var name=undefined;
            if(ref['person-group'][i].hasOwnProperty('name')){
                name='name';
            }
            else if(ref['person-group'][i].hasOwnProperty('string-name')){
              name='string-name';
            }
            
            if(ref['person-group'][i].hasOwnProperty('name')){
              

              if(Array.isArray(ref['person-group'][i]['name']))
              {
                //console.log(ref['person-group'][i]['name']);
                var s="";
                
                for(var j=0;j<ref['person-group'][i]['name'].length;j++){
                  var gname=(typeof(ref['person-group'][i]['name'][j]['given-names'])=='object')?ref['person-group'][i]['name'][j]['given-names']['#text']:ref['person-group'][i]['name'][j]['given-names'];
                  var sname=(typeof(ref['person-group'][i]['name'][j]['surname'])=='object')?ref['person-group'][i]['name'][j]['surname']['#text']:ref['person-group'][i]['name'][j]['surname'];
                  if(j==ref['person-group'][i]['name'].length-1){
                    s=s.slice(0, -1);
                    //console.log(s);
                    
                  s+=" and "+sname+" "+gname+".";
                  }
                  else{
                    s+=sname+" "+gname+".,";
                  }
              }
              var type=ref['person-group'][i]['person-group-type'];
              newobj2.push({[type]:s});
            }
            else{
              
              var sname=(typeof(ref['person-group'][i]['name']['surname'])=='object')?ref['person-group'][i]['name']['surname']['#text']:ref['person-group'][i]['name']['surname'];
              var type=ref['person-group'][i]['person-group-type'];
              var gname=(typeof(ref['person-group'][i]['name']['given-names'])=='object')?ref['person-group'][i]['name']['given-names']['#text']:ref['person-group'][i]['name']['given-names'];
              newobj2.push({[type]:sname+" "+gname+"."});
            }
        
        
        }
        else{
            var collab=(typeof(ref['person-group']['collab'])=="object" && !Array.isArray(ref['person-group']['collab']))?ref['person-group']['collab']['#text']:ref['person-group']['collab'];
          
            if(Array.isArray(collab))
              {
                
                var s="";
                
                for(var j=0;j<collab.length;j++){
                  var ncollab=(typeof(collab[j])=="object")?collab[j]["#text"]:collab[j];
                  if(j==collab.length-1)
                  {
                    s+=ncollab;
                  }
                  else
                  {
                    s+=ncollab+", ";
                  }
              }
              var type=ref['person-group'][i]['person-group-type'];
              newobj.push({[type]:s});
            }
            else
            {
                var type=ref['person-group'][i]['person-group-type'];
                newobj.push({[type]:collab});
            }
        }
    }
    
    
    newobj.push(Object.assign({}, ...newobj2));
  }
      else if(ref.hasOwnProperty('person-group')){
        
        
        if(ref['person-group'].hasOwnProperty('name')){
        if(ref['person-group']['name'].length)
           {
             
             var s="";
             
          for(var j=0;j<ref['person-group']['name'].length;j++){
           
              var gname=(typeof(ref['person-group']['name'][j]['given-names'])=='object')?ref['person-group']['name'][j]['given-names']['#text']:ref['person-group']['name'][j]['given-names'];
              var sname=(typeof(ref['person-group']['name'][j]['surname'])=='object')?ref['person-group']['name'][j]['surname']['#text']:ref['person-group']['name'][j]['surname'];
              if(j==ref['person-group']['name'].length-1){
                s=s.slice(0, -2);
                s+=" and "+sname+" "+gname+".";
              }
              else{
                  s+=sname+" "+gname+"., ";
              }
           }
            var type=ref['person-group']['person-group-type'];
            newobj.push({[type]:s});
         }
         else{
            var type=ref['person-group']['person-group-type'];
            var gname=(typeof(ref['person-group']['name']['given-names'])=='object')?ref['person-group']['name']['given-names']['#text']:ref['person-group']['name']['given-names'];
            var sname=(typeof(ref['person-group']['name']['surname'])=='object')?ref['person-group']['name']['surname']['#text']:ref['person-group']['name']['surname'];
            newobj.push({[type]:sname+" "+gname+"."});
          }
        }
        else{
          var collab=(typeof(ref['person-group']['collab'])=="object" && !Array.isArray(ref['person-group']['collab']))?ref['person-group']['collab']['#text']:ref['person-group']['collab'];
        
          if(Array.isArray(collab))
             {
               
               var s="";
               
               for(var j=0;j<collab.length;j++){
                  var ncollab=(typeof(collab[j])=="object")?collab[j]["#text"]:collab[j];
                  if(j==collab.length-1){
                      // s=s.slice(0,-1);
                      s+=ncollab;
                  }
                  else
                  {
                      s+=ncollab+", ";
                  }
             }
             var type=ref['person-group']['person-group-type'];
             newobj.push({[type]:s});
           }
           else{
              var type=ref['person-group']['person-group-type'];
              newobj.push({[type]:collab});
            }
        }
      }
      else{
          newobj.push({author:undefined});
      }
    }

    return newobj;
      }

exports.convertSource=function(reference_list){           // Function for adding titles and comments, reference_list has the xml data in json format that we filtered containing all the information required to convert......
      var newobj=new Array();
      for(var m=0;m<reference_list.length;m++){
        //console.log(reference_list[m]);
        try{
            if(reference_list[m]['element-citation']['publication-type']=='confproc'){            // IF publication type is conference
              var chapterTitle=reference_list[m]['element-citation']['chapter-title'];
              var title=(typeof(reference_list[m]['element-citation']['conf-name'])=="object")?reference_list[m]['element-citation']['conf-name']["#text"]:reference_list[m]['element-citation']['conf-name'];
              var Art_title=(typeof(reference_list[m]['element-citation']['article-title'])=="object")?reference_list[m]['element-citation']['article-title']["#text"]:reference_list[m]['element-citation']['article-title'];
              var conf_loc= (typeof(reference_list[m]['element-citation']['conf-loc']=="object"))?reference_list[m]['element-citation']['conf-loc']['#text']:reference_list[m]['element-citation']['conf-loc'];
              if(chapterTitle && chapterTitle['italic']){
                var ctitle=chapterTitle['italic']['#text'];
              }
              else{
                var ctitle=(typeof(chapterTitle)=="object")?chapterTitle['#text']:chapterTitle;
              }
              if(reference_list[m]['element-citation']['comment']){
        
                if(Array.isArray(reference_list[m]['element-citation']['comment'])){
                  var notes=new Array();
                  for(var i=0;i<reference_list[m]['element-citation']['comment'].length;i++){
                    if(reference_list[m]['element-citation']['comment'][i]['data-class']=='RefComments'){
                      notes.push(reference_list[m]['element-citation']['comment'][i]['#text']);
                      }
                  }
                  newobj.push({"container-title":title,"title":[Art_title,ctitle],"note":notes,"location":conf_loc});
                }
                else{
                    var cmnt=(reference_list[m]['element-citation']['comment']['data-class']=='RefComments')?reference_list[m]['element-citation']['comment']["#text"]:undefined;
                    newobj.push({"container-title":title,"title":[Art_title,ctitle],"location":conf_loc,"note":cmnt});
                }
              }
              else{
                  
                  newobj.push({"container-title":title,"title":[Art_title,ctitle],"location":conf_loc});
              }
            }
            else if(reference_list[m]['element-citation']['publication-type']=='book' || reference_list[m]['element-citation']['publication-type']=='thesis'){
              var chapterTitle=reference_list[m]['element-citation']['chapter-title'];
              var source=reference_list[m]['element-citation']['source'];
             
              
              
              var ctitle=(typeof(chapterTitle)=="object")?chapterTitle['#text']:chapterTitle;
              
              if(source['italic']){                                                                           // Not sure what else can come other than Italics...
                
                var title=source['italic']['#text'];
              }
              else{
                console.log("============================================================");
                console.log(source);
              var title=(typeof(source)=="object")?source['#text']:source;
              }
              if(reference_list[m]['element-citation']['comment'] ){
                
                  if(Array.isArray(reference_list[m]['element-citation']['comment'])){
                    var notes=new Array();
                    for(var i=0;i<reference_list[m]['element-citation']['comment'].length;i++){
                      if(reference_list[m]['element-citation']['comment'][i]['data-class']=='RefComments'){
                      notes.push(reference_list[m]['element-citation']['comment'][i]['#text']);
                      }
                    }
                    newobj.push({"container-title":title,"note":notes,"title":ctitle});
                  }
                  else{
                      var cmnt=(reference_list[m]['element-citation']['comment']['data-class']=='RefComments')?reference_list[m]['element-citation']['comment']["#text"]:undefined;
                      newobj.push({"container-title":title,"note":cmnt,"title":ctitle});
                  }
                }
                else{
              newobj.push({"container-title":title,"title":ctitle});
                }
            }
            else if(reference_list[m]['element-citation']['publication-type']=='software'){
              
              var title=(typeof(reference_list[m]['element-citation']['data-title'])=="object")?reference_list[m]['element-citation']['data-title']["#text"]:reference_list[m]['element-citation']['data-title'] ;
            
              newobj.push({"title":title});
            }
            else {
              var Art_title=(typeof(reference_list[m]['element-citation']['article-title'])=="object")?reference_list[m]['element-citation']['article-title']["#text"]:reference_list[m]['element-citation']['article-title'];
              if(reference_list[m]['element-citation']['comment'] ){
                if(Array.isArray(reference_list[m]['element-citation']['comment'])){
                  var notes=new Array();
                  for(var i=0;i<reference_list[m]['element-citation']['comment'].length;i++){
                    if(reference_list[m]['element-citation']['comment'][i]['data-class']=='RefComments'){
                      notes.push(reference_list[m]['element-citation']['comment'][i]['#text']);
                      }
                  }
                  newobj.push({"title":Art_title,"note":notes});
                }else{
                  var cmnt=(reference_list[m]['element-citation']['comment']['data-class']=='RefComments')?reference_list[m]['element-citation']['comment']["#text"]:undefined;
                  newobj.push({"title":Art_title,"note":cmnt});
                }
              }
              else{
              newobj.push({"title":Art_title});
              }
            }
        }
      
    
        catch{
          newobj.push({"title":undefined});
        }
      }
      return newobj;
}     
exports.convertPublisher=function(reference_list){        // Function for adding dates, publisher information, doi, page number,edition, genre and etc,  reference_list has the xml data in json format that we filtered containing all the information required to convert......
    var newobj=new Array();
    for(var m=0;m<reference_list.length;m++){
      //initializing values ...
        var publisher=(typeof(reference_list[m]['element-citation']['publisher-name'])=="object")?reference_list[m]['element-citation']['publisher-name']["#text"]:reference_list[m]['element-citation']['publisher-name'];
        var location=(typeof(reference_list[m]['element-citation']['publisher-loc'])=="object")?reference_list[m]['element-citation']['publisher-loc']["#text"]:reference_list[m]['element-citation']['publisher-loc'];
        var volume=(typeof(reference_list[m]['element-citation']['volume'])=="object")?reference_list[m]['element-citation']['volume']["#text"]:reference_list[m]['element-citation']['volume'];
        var doi=undefined;
        var url=undefined;
        var edition=undefined;
        var genre=undefined;
        var date=undefined;
        var issue=reference_list[m]['element-citation']['issue'];
        var isbn=undefined;
        var pages=undefined;
        if(reference_list[m]['element-citation']['fpage'] && reference_list[m]['element-citation']['lpage']){
            var fpage=(typeof(reference_list[m]['element-citation']['fpage'])=="object")?reference_list[m]['element-citation']['fpage']["#text"]:reference_list[m]['element-citation']['fpage'];
            var lpage=(typeof(reference_list[m]['element-citation']['lpage'])=="object")?reference_list[m]['element-citation']['lpage']["#text"]:reference_list[m]['element-citation']['lpage'];
            pages=`${fpage}-${lpage}`;
        }
        if(reference_list[m]['element-citation']['comment']){
            
            genre=(reference_list[m]['element-citation']['comment']['data-class']=="RefGenre")?reference_list[m]['element-citation']['comment']['#text']:undefined;
            if(reference_list[m]['element-citation']['publication-type']=='web' || reference_list[m]['element-citation']['publication-type']=='website'){
              var dayAccessed=undefined;
              var MonthAccessed=undefined;
              var YearAccessed=undefined;
              var comments=reference_list[m]['element-citation']['comment'];
              dayAccessed=(typeof(reference_list[m]['element-citation']['span'])=='object')?(reference_list[m]['element-citation']['span'][0]['data-class']=='RefAccesedDay')?reference_list[m]['element-citation']['span'][0]['#text']:(reference_list[m]['element-citation']['span'][1]['data-class']=='RefAccesedDay')?reference_list[m]['element-citation']['span'][1]['#text']:undefined:undefined;
              MonthAccessed=(typeof(reference_list[m]['element-citation']['span'])=='object')?(reference_list[m]['element-citation']['span'][0]['data-class']=='RefAccesedMonth')?reference_list[m]['element-citation']['span'][0]['#text']:(reference_list[m]['element-citation']['span'][1]['data-class']=='RefAccesedMonth')?reference_list[m]['element-citation']['span'][1]['#text']:undefined:undefined;
              if(Array.isArray(comments)){
                for(var i=0;i<comments.length;i++){
                  if(comments[i]['data-class']=="RefLAD"){
                    YearAccessed=comments[i]['#text'];
                  }
                }
              }
              else{
                YearAccessed=(comments['data-class']=="RefLAD")?reference_list[m]['element-citation']['comment']['#text']:undefined;
              }
             
              
              date="";
              if(dayAccessed!=undefined){
                //console.log(dayAccessed);
                date=String(dayAccessed);
              }
              if(MonthAccessed!= undefined){
                date+=" "+MonthAccessed;
              }
              if(YearAccessed!=undefined){
                date+=" "+YearAccessed;
              }  
            
          }
        }
        if(reference_list[m]['element-citation']['publication-type']=='confproc' && reference_list[m]['element-citation']['span']){
          var day=null;
          var month=null;
          var year=null;
          
          //console.log(reference_list[m]['element-citation']['span']);
          for(var i=0;i<reference_list[m]['element-citation']['span'].length;i++){
            
              if(reference_list[m]['element-citation']['span'][i]['data-class']=="RefPaperNumber"){
                console.log("===========================================================");
                var pages2=reference_list[m]['element-citation']['span'][i]['#text'];
                console.log(pages2);
              }
              if(reference_list[m]['element-citation']['span'][i]['data-class']=="RefEventDate")
              {
                if(day){
                  
                  var x=new Array();
                  x.push(day);
                  
                  //console.log(day);
                  day=x;
                  day.push(reference_list[m]['element-citation']['span'][i]['#text']);
                }
                else{
                  day=reference_list[m]['element-citation']['span'][i]['#text'];
                }
              }
              else if(reference_list[m]['element-citation']['span'][i]['data-class']=="RefEventMonth"){
                  month=reference_list[m]['element-citation']['span'][i]['#text']
              }
              else if(reference_list[m]['element-citation']['span'][i]['data-class']=="RefEventYear")
              {
                  year=reference_list[m]['element-citation']['span'][i]['#text']
              }
        
          }
          
          if(day){
            if(Array.isArray(day)){
              // console.log("===========================================================");
              // console.log(day);
              day=`${day[0]}-${day[day.length-1]}`;
            }
            else{
              day=String(day);
            }
            date=day;
          }
          if(month){
            date+=" "+month;
          }
          if(year){
            date+=" "+year;
          }  
        
      }
      if(reference_list[m]['element-citation']['year'] ){
        if(date){
            var date=new Array(date);
            date.push(reference_list[m]['element-citation']['year']['#text']);
        }
          else{
          
            date=reference_list[m]['element-citation']['year']['#text'];
        }
      }
      if(reference_list[m]['element-citation']['span'] && reference_list[m]['element-citation']['span']['RefPaperNumber']){
        
          var pages2=reference_list[m]['element-citation']['span']['#text'];
        
      }
      if(reference_list[m]['element-citation']['uri']){
        url=reference_list[m]['element-citation']['uri']["#text"];
      }
        
      if(reference_list[m]['element-citation']['pub-id']){
          
        doi=(reference_list[m]['element-citation']['pub-id']['pub-id-type']=='doi')?reference_list[m]['element-citation']['pub-id'][ '#text']:undefined;
        isbn=(reference_list[m]['element-citation']['pub-id']['pub-id-type']=='pmid')?reference_list[m]['element-citation']['pub-id'][ '#text']:undefined;
      }
        if(reference_list[m]['element-citation']['publication-type']=='book'){
          edition=(typeof(reference_list[m]['element-citation']['edition'])=="object")?reference_list[m]['element-citation']['edition']["#text"]:reference_list[m]['element-citation']['edition'];
      }
      
      newobj.push({date:date,publisher:publisher,location:location,volume:volume,edition:edition,url:url,doi:doi,issue:issue,genre:genre,isbn:isbn,pages:[pages,pages2]});
      
    }
    return newobj;
}