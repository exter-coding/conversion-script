const { XMLBuilder} = require('fast-xml-parser');
const fs = require('fs');
const { Testfile } = require('./Test');
exports.fetchedReferences=function(source,reference_list){   
    fs.readdir(source, function (err, files) {
      
      if (err) {                                                             //handling error
          return console.log('Unable to scan directory: ' + err);
      } 
      
      files.forEach(async function (file) {                                  //listing all files using forEach
          
        
      
        fs.readFile(`${source}/${file}` , 'utf8' ,async (err, data) => {
            if (err) {
              console.error(err);
              return;
            }        


          
          const refs = new XMLBuilder();                     
          const xmlrefs = refs.build(reference_list);
          //console.log(xmlrefs);

          //console.log("\n\n\n<=====================================================>\n\n\n")
          fs.writeFileSync(`${source}/${file.slice(0,-4)}_references_fetched.xml`, xmlrefs, function(err) {  //storing output for file.xml as foldername_output.xml
            if (err) throw err;
        });
        });
        
    });
});
Testfile(source,reference_list);
}
