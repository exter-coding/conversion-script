const { XMLParser,XMLBuilder} = require('fast-xml-parser');
const fs = require('fs')
const {convertAuthor,convertSource,convertPublisher}=require('./conversion');
const getReferences=require('./fetch_reference');
exports.Anystyle=async function(folderName){                                // function to take input from the user in kriyadocs format and produce output in anystyle xml format folderName parameter to get the folderName storing the extracted data 
  var reference_list=new Array();

  const source=`./${folderName}`;
  
  fs.readdir(source, function (err, files) {
      
      if (err) {                                                             //handling error
          return console.log('Unable to scan directory: ' + err);
      } 
      
      files.forEach(async function (file) {                                  //listing all files using forEach
          
        
      
        fs.readFile(`./${folderName}/${file}` , 'utf8' ,async (err, data) => {
            if (err) {
              console.error(err);
              return;
            }
            //console.log(data);
          
            const options = {
              ignoreAttributes: false,
              attributeNamePrefix : ""
              


          };
          const parser = new XMLParser(options);

          let jObj = await parser.parse(data);                                // Converting the XML format to json ...
          //console.log(jObj);
          var references=jObj['article']['back']['ref-list']['ref'];          // Looking for 'ref' tag in kriyadocs xml 


          for(var i=0;i<references.length;i++){

            var x=(references[i]['element-citation'])?references[i]['element-citation']:(references[i]['mixed-citation'])?references[i]['mixed-citation']:undefined;

            if(x && x['publication-type']){
                    //console.log(x['comment']);
            if(x['publication-type']=="book" || x['publication-type']=="web" || x['publication-type']=="thesis"|| x['publication-type']=="software"||x['publication-type']=="report" ||x['publication-type']=="thesis"|| x['publication-type']=="website"||x['publication-type']=="confproc" ){
                    reference_list.push(references[i]);

            }
            }
          }
          getReferences.fetchedReferences(source,reference_list);
         
          //console.log(xmlrefs);
          var authors=convertAuthor(reference_list);                 // getting authors in anystyle format (json)



          var titles=convertSource(reference_list);                 // getting titles in anystyle format (json)

          var publisher_details=convertPublisher(reference_list);   // getting publishers information along with other information in anystyle format (json)

          var sequence=new Array();                               
          for(var i=0;i<reference_list.length;i++){
              var obj=Object.assign(authors[i],publisher_details[i],titles[i]);
              sequence.push(obj);
          }
          console.log(sequence);
          const builder = new XMLBuilder();                     
          const xmlContent = builder.build({sequence:sequence});      //converting json to xml format ...
          fs.writeFileSync(`./${folderName}/${file.slice(0,-4)}_output.xml`, xmlContent, function(err) {  //storing output for file.xml as foldername_output.xml
              if (err) throw err;
          });
        });

        //console.log(file); 
    });
  });
}







